import axios from 'axios'


const config = require('../config/config.js');

export default() => {
  return axios.create({
    baseURL: `${global.gConfig.rest_endpoint}`
  })
}